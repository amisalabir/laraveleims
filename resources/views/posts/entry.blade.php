<?php 
namespace App\Http\Controllers; 
use App\Http\Controllers\PostsController; 
 $objEims=new PostsController();
?>
@extends('layouts.app')
@section('content')
@include('includes.header')
@guest
@else

<div class="content">
 <div class="container">
        <div class="row">  
         <!-- LARAVEL TEST -->
        </div>
        <div class="container"><br></div>
    <div class="row">
        <form class="form-group" name="transactionEntry" action="{{ route('posts.insert') }}" method="post">
            {{ csrf_field() }}
        <input hidden name="addTransaction" type="text" value="addTransaction">
        <input name="modifiedDate" type="text" hidden  value="<?php echo date('Y-m-d');?>">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="transactionType">Transaction Type :</label> </div>
                    <div class="col-sm-4 text-left">
                        <select id="transactionType" name="transactionType" onchange="checkTransactionType(this.value)" style="width:150px; " class="form-control">

                    <?php   
                    $entrytype=$objEims->selectView('entrytype'); 
                    ?>
                        @foreach($entrytype as $singletype)

                        <option value='{{$singletype->labelvalue}}'> {{$singletype->name}} </option>

                        @endforeach

                        </select></div>
                    <div class="col-sm-4"></div>
                </div>
                
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedClient">Branch :</label> </div>
                    <div class="col-sm-6 text-left ">
                        <select  name="branchid" style="width:auto; " class="form-control text-uppercase ">
                            <?php

                        $allData=$objEims->selectView('branch');        

                    ?>
                            @foreach($allData as $singleData)
                              <option value='{{$singleData->id}}'> {{$singleData->name}} </option>
                             @endforeach
                     </select>
                   </div>

                    <div class="col-sm-2"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="accountHead">Account Head :</label> </div>
                    <div id="accountHead" class="col-sm-3 text-left ">
                     <select id="accheadId"   data-live-search="true"  class="selectpicker text-uppercase"  name="accheadId" onchange="CheckAccountHeadField(this.value)"  >
                    <?php      
                    $allData=$objEims->selectView('acchead');        
                    ?> @foreach($allData as $singleData) <option value='{{$singleData->id}}'> {{$singleData->name}}({{$singleData->id}}) </option> @endforeach
                     </select>
                    </div>
                  <div id="productId" class="col-sm-3">

                  </div>
                  <div class="col-sm-2"></div>
                </div>
                <div id="weight" style='display:;'>

                </div>
                <div id="activeBankField"   style='display:none;'>
                    <div class="row">
                    <div class="col-sm-4 text-right form-group "><label for="relatedBank">Account Name :</label> </div>
                    <div class="col-sm-4 text-left ">
                        <select id="bankId"  name="bankId"  class="form-control text-uppercase ">
                            <option class="text-uppercase" value="0">SELECT BANK</option>
                      <?php      
                    $allData=$objEims->selectView('bank');        
                    ?> @foreach($allData as $singleData) <option value='{{$singleData->id}}'> {{$singleData->accountname}} ({{$singleData->id}} - {{$singleData->name}}) </option> @endforeach
                        </select>
                    </div>
                    
                    <div class="col-sm-4"></div>
                </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionDate"> Date :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control selectDate" id="transactionDate" name="transactionDate" required placeholder="yyyy-mm-dd" onchange="" onkeypress="" type="text">
                    </div>
                    <div class="col-sm-7"></div>
                </div>
                <div id="voucherNo"  class="row" style="display:block;">
                    <div class="col-sm-4 text-right form-group"><label for="voucherNo"> Voucher No (DR) :</label></div><div class="col-sm-2 text-left"><input class="form-control" value="0" name="voucherNo" type="text" ></div><div class="col-sm-6"></div>
                </div>

                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="transactionMode">Transaction Method :</label></div>
                    <div class="col-sm-2 text-left">
                        <select id="transactionMode" name="transactionMode" class="form-control" onchange='ChequeField(this.value);' required>
                            <option selected="selected" value="CASH">CASH</option>
                            <option value="CASH CHEQUE">CASH CHEQUE</option>
                            <option value="A/C PAYEE CHEQUE">A/C PAYEE CHEQUE</option>
                            <option value="ONLINE TRANSFER">ONLINE TRANSFER</option>
                            <option value="MOBILE BANK">MOBILE BANK</option>
                            <option value="PAY ORDER">PAY ORDER</option>
                            <option value="ATM">ATM</option>
                            <option value="D.D.">D.D.</option>
                            <option value="T.T.">T.T.</option>
                            <option value="OTHERS">OTHERS</option>
                        </select>
                        <!-- Transaction Mehtod should be called from DB-->
                    </div>
                    <div class="col-sm-6"></div>
                </div>
                <div class="row" id="paidTo"  >
                    <div class="col-sm-4 text-right form-group"><label for="receivedTo">Paid To :</label></div><div class="col-sm-3 text-left"><input class="form-control" name="receivedTo" type="text" ></div><div class="col-sm-5"></div>
                </div>
                <div id="activeChequeField" style='display:;'>

                </div>
                <div class="row">

                    <div class="col-sm-4 text-right form-group"><label for="remarks"> Remarks :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control"  name="remarks" rows="2" cols="20"  ></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="txtAmount"> Amount :</label></div>
                    <div class="col-sm-3 text-left">
                        <input class="form-control" name="amount"  id="txtAmount" required type="text">
                    </div>
                    <div class="col-sm-5"></div>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-right form-group"><label for="inWords">In Words :</label></div>
                    <div class="col-sm-5 text-left">
                        <textarea class="form-control" name="inWords"></textarea>
                    </div>
                    <div class="col-sm-3"></div>
                </div>
            </div>
                <div class="row">
                    <div class="col-sm-5"></div>
                    <div class="col-sm-3 text-right form-group">
                       <!-- <button  type="submit" class="btn btn-primary form-control">Submit</button>-->
                        <input type="submit" class="btn-primary form-control" value="Submit">
                    </div>
                    <div class="col-sm-4"></div>
                </div>
                <div class="col-sm-1"></div>
        </div>
        </form>
    </div>
 </div>
</div>
Test
@endguest
@include('includes.footer')
@endsection

