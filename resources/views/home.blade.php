@extends('layouts.app')
@section('content')
@include('includes.header')
/* Timely programming*/
<div class="container">
    <div class="row">
<!-- A new comment  for server option has been added 

-->
<div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">

                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>

   
        </div>
    </div>
@include('includes.footer')
@endsection
