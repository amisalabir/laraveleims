 @guest
 @else
<div class="container headSection">
        <div class="row">
       
                    <div class="col-sm-12 text-left title">
                       <h4>Expense & Income Management System.</h4>
                    </div>
        
        </div>
         <div class="row">
       
                    <div class="col-sm-12 text-right title">
                       <input type='text' name='search'/>
                    </div>
        
        </div>
        <div class="row">
                
                <nav  class="navbar navbar-default navbar-static">
                        <div style="padding-left:0px; padding-right:0px;" class="container">
                            <div style="padding-top:0px; margin:0px;" class="">
                            </div>
                            <div style="padding-top:0px; margin-top:0px;" class="navbar-header">
                              <button  type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>                        
                              </button>
                              <!--<a class="navbar-brand" href="#myPage"><h4>BSBL</h4></a>-->
                            </div>
                            <div style="padding-top:0px; margin-top:0px;"  class="collapse navbar-collapse" id="myNavbar">
                                <div style="padding-left:0px; padding-right:0px;" class="container">
                                  <ul class="nav navbar-nav navbar-left">
                                    <li class="backg"><a href="{{ route('home') }}">HOME</a></li>
                                    <li class="dropdown">
                                        <a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">ACCOUNT INPUTS<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li class="backg"><a id="menu" href="{{ route('posts.entry') }}">Single Transaction Entry</a></li>
                                            <li class="backg"><a id="menu" href="other/multipleVoucher.php">Multiple Voucher Entry</a></li>
                                            <li class="divider"></li>
                                            <li class="backg"><a id="menu" href="create.php?id=addStudent">Add Student</a></li>
                                            <li class="backg"><a id="menu" href="create.php?id=addShead">Add Account Head</a></li>
                                            <li class="backg"><a id="menu" href="view.php?id=viewhead">View Head</a></li>
                                            <li class="backg"><a id="menu" href="create.php?id=addBank">Add Bank</a></li>
                                            <li class="backg"><a id="menu" href="addParty.php">Add Party</a></li>

                                        </ul>
                                    </li>

                                    <li class="dropdown">
                                        <a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">ACOUNT REPORTS<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li class="backg"><a id="menu" href="statement.php?branchid=currentMonth">Cash Book</a></li>
                                            <li class="backg"><a id="menu" href="#">Bank Book</a></li>
                                            <li class="backg"><a id="menu" href="#">Ledger Book</a></li>
                                            <li class="backg"><a id="menu" href="#">Trial Balance</a></li>
                                            <li class="backg"><a id="menu" href="#">Trading Account</a></li>
                                            <li class="backg"><a id="menu" href="#">P/L Account</a></li>
                                            <li class="backg"><a id="menu" href="#">Statement of Affairs</a></li>
                                            <li class="backg"><a id="menu" href="opneningbalance.php">Opnening Balance</a></li>
                                            <li class="divider"></li>

                                        </ul>
                                    </li>
                                    <?php /*
                                    if ($singleUser->role=='admin'){
                                        echo "<li class=\"dropdown\">
                                        <a id=\"menu\" href=\"\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" data-hover=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" data-animations=\"zoomIn zoomIn zoomIn zoomIn\">SETTINGS <span class=\"caret\"></span></a>
                                        <ul class=\"dropdown-menu\">
                                            <li class=\"backg\"><a id=\"menu\" href=\"create.php?id=user\">Create User</a></li>
                                            <li class=\"backg\"><a id=\"menu\" href=\"#\">Import Data</a></li>
                                        </ul>
                                    </li>";
                                    } */
                                    ?>

                                  </ul>
                                </div>
                            </div>
                        </div>
                </nav>
        </div>
    </div>
    @endguest