

<script type="text/javascript">

    $('.from-datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });
    $('.to-datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });

    $(document).ready(function () {
        calculate();
    });

    function calc() {

        var totalamount=((parseFloat($('#rate').val(), 10)*parseFloat($("#bandwidth").val(), 10)) /30 )* parseFloat($("#totalDays").val(), 10)
        $('.totalamount').val(totalamount);
        /*
        $('#amount').html(
            (parseFloat($('#rate').val(), 10) /30 )* parseFloat($("#totalDays").val(), 10)

        );
        */

    }
    $("#rate").keyup(calc);
    $("#bandwidth").keyup(calc);
    $("#bandwidth").change(calc);
    //$("#from").keyup(calc);
    $("#fromDurationDate").change(calc);
    $("#toDurationDate").change(calc);
    $("#totalDays").onload(calc);
    $("#totalDays").change(calc);

</script>

<script>
    $('.fromdate').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });
    $('.todate').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });

    $('.selectDate').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
    });


    $('.fromdate').datepicker().bind("change", function () {
        var minValue = $(this).val();
        minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
        $('.todate').datepicker("option", "minDate", minValue);
        calculate();
    });
    $('.todate').datepicker().bind("change", function () {
        var maxValue = $(this).val();
        maxValue = $.datepicker.parseDate("yy-mm-dd", maxValue);
        $('.fromdate').datepicker("option", "maxDate", maxValue);
        calculate();
    });

    function calculate() {
        var d1 = $('.fromdate').datepicker('getDate');
        var d2 = $('.todate').datepicker('getDate');
        var oneDay = 24*60*60*1000;
        var diff = 0;
        if (d1 && d2) {

            diff = Math.round(Math.abs((d2.getTime() - d1.getTime())/(oneDay)));
        }
        $('.calculated').val(diff+1);
        //$('.minim').val(d1)
    }

</script>
<!-- dropdown search-->
<!-- Placed at the end of the document so the pages load faster -->

<script src="{{ asset('resource/bootstrap/js/jquery.js')}}"> </script>
<script src="{{ asset('resource/js/bootstrap-dropdownhover.min.js')}}"></script>
<script src="{{ asset('resource/js/addtransaction.js')}}"></script>
<script src="../resource/select2/dist/js/select2.js"></script>
<!--
    <script src="{{ asset('resource/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js')}}"></script>
   <script src="../resource/select2/jquery-3.2.1.min.js"></script>
    <script src="{{ asset('resource/js/jquery.min.js')}}"></script>
    <script src="{{ asset('resource/js/ie10-viewport-bug-workaround.js')}}"></script>
-->

<!-- clock
<script src="../resource/clock/js/jquery-1.10.2.min.js"></script>
<script src="../resource/clock/js/bootstrap.min.js"></script>
-->
<script type="text/javascript">
    /* Set the width of the side navigation to 250px */
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    /* Set the width of the side navigation to 0 */
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>



<script>
    jQuery(function($) {
        $('#message').fadeIn(500);
        $('#message').fadeOut (500);
        $('#message').fadeIn (500);
        $('#message').delay (2500);
        $('#message').fadeOut (2000);
    })

    $('#delete').on('click',function(){
        document.forms[1].action="deletemultiple.php";
        $('#multiple').submit();
    });

    //select all checkboxes
    $("#select_all").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('.checkbox').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('.checkbox').change(function(){ //".checkbox" change
//uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#select_all")[0].checked = false; //change "select all" checked status to false
        }

//check "select all" if all checkbox items are checked
        if ($('.checkbox:checked').length == $('.checkbox').length ){
            $("#select_all")[0].checked = true; //change "select all" checked status to true
        }
    });


</script>



<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

           
           //$comma_separated_keywords;
            
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });

        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>


<?php
/* Clock */
$date = new DateTime();
$current_timestamp = $date->getTimestamp();
?>
<script>
    flag_time = true;
    timer = '';
    setInterval(function(){phpJavascriptClock();},1000);

    function phpJavascriptClock()
    {
        if ( flag_time ) {
            timer = <?php echo $current_timestamp;?>*1000;
        }
        var d = new Date(timer);
        months = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec');

        month_array = new Array('January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'Augest', 'September', 'October', 'November', 'December');

        day_array = new Array( 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

        currentYear = d.getFullYear();
        month = d.getMonth();
        var currentMonth = months[month];
        var currentMonth1 = month_array[month];
        var currentDate = d.getDate();
        currentDate = currentDate < 10 ? '0'+currentDate : currentDate;

        var day = d.getDay();
        current_day = day_array[day];
        var hours = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();

        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour ’0′ should be ’12′
        minutes = minutes < 10 ? '0'+minutes : minutes;
        seconds = seconds < 10 ? '0'+seconds : seconds;
        var strTime = hours + ':' + minutes+ ':' + seconds + ' ' + ampm;
        timer = timer + 1000;
        //document.getElementById("demo").innerHTML= currentMonth+' ' + currentDate+' , ' + currentYear + ' ' + strTime ;
        //document.getElementById("demo1").innerHTML= currentMonth1+' ' + currentDate+' , ' + currentYear + ' ' + strTime ;
        //document.getElementById("demo2").innerHTML= currentDate+':' +(month+1)+':' +currentYear + ' ' + strTime ;
        //document.getElementById("demo3").innerHTML= strTime ;
        document.getElementById("demo4").innerHTML= current_day + ' , ' +currentMonth1+' ' + currentDate+' , ' + currentYear + ' ' + strTime ;
        flag_time = false;
    }
</script>
<!-- clock ended -->
<!-- Live select search -->

<script type="text/javascript">

/*
 var options = {
  values: "a, b, c",
  ajax: {
    url: "ajax.php",
    type: "POST",
    dataType: "json",
    // Use "{{ asset('{{{q}}}')}}" as a placeholder and Ajax Bootstrap Select will
    // automatically replace it with the value of the search query.
    data: {
      q: "{{ asset('{{{q}}}')}}"
    }
  },
  locale: {
    emptyTitle: "Select and Begin Typing"
  },
  log: 3,
  preprocessData: function(data) {
    var i,
      l = data.length,
      array = [];
    if (l) {
      for (i = 0; i < l; i++) {
        array.push(
          $.extend(true, data[i], {
            text: data[i].Name,
            value: data[i].Email,
            data: {
              subtext: data[i].Email
            }
          })
        );
      }
    }
    // You must always return a valid array when processing data. The
    // data argument passed is a clone and cannot be modified directly.
    return array;
  }
};

*/    


$(".selectpicker")
  .selectpicker()
  .filter(".with-ajax")
  .ajaxSelectPicker(options);
$("select").trigger("change");

function chooseSelectpicker(index, selectpicker) {
  $(accheadId).val(index);
  $(accheadId).selectpicker('refresh');
}
</script>

</body>
</html>

