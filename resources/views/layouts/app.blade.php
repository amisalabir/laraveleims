<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'EIMS') }}</title>
    <!-- Search in Drop down -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>
    <script type="text/javascript" src="{{ asset('resource/bootstrap/js/daterangepicker.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('resource/bootstrap/css/daterangepicker.css')}}" />
      <!-- Bootstrap core CSS -->
    <link href="{{ asset('resource/css/style.css')}}" rel="stylesheet" type="text/css">
      <link href="{{ asset('resource/css/bootstrap.min.css')}}" rel="stylesheet" media="screen">
      <link href="{{ asset('resource/css/animate.min.css')}}" rel="stylesheet">
      <link href="{{ asset('resource/css/bootstrap-dropdownhover.min.css')}}" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('resource/bootstrap/css/jquery-ui.css')}}">
      <script src="{{ asset('resource/bootstrap/js/jquery.js')}}"></script>
      <script src="{{ asset('resource/bootstrap/js/jquery-ui.js')}}"></script>
      <script src="{{ asset('resource/js/index.js')}}"></script>
      <link href="{{ asset('resource/select2/dist/css/select2.css')}}" rel='stylesheet' type='text/css'>

      <!--   
          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
      <link href='../resource/clock/css/style.css' rel='stylesheet' type='text/css' media="screen">
      -->
  <style type="text/css">
        .bootstrap-select {  width: 100% !important; }
  </style>       

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'EIMS') }}
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav"></ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li class="backg" >
                                        <a id="menu" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')    
    </div>



    