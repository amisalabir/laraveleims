<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255);
            $table->string('adress',255);
            $table->date('fy_start');
            $table->date('fy_end');
            $table->string('currency_symbol',100);
            $table->string('currency_format',100);
            $table->string('user_agent',100);
            $table->integer('decimal_places');
            $table->string('date_format',100);
            $table->string('time_zone',100);
            $table->integer('manage_inventory');
            $table->integer('account_locked');
            $table->integer('email_use_default');
            $table->string('email_protocol',10);
            $table->string('email_host',255);
            $table->integer('port');
            $table->integer('tls');
            $table->string('email_username',255);
            $table->string('email_password',255);
            $table->string('email_from',255);
            $table->decimal('print_paper_height',10,3);
            $table->decimal('print_paper_width',10,3);
            $table->decimal('print_margin_top',10,3);
            $table->decimal('print_margin_bottom',10,3);
            $table->decimal('print_margin_left',10,3);
            $table->decimal('print_margin_right',10,3);
            $table->string('print_orientation',1);
            $table->string('print_page_formt',1);
            $table->integer('database_version');
            $table->binary('settings');
            $table->string('softdelete',3);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('settings');
    }
}
