<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entrytypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrytypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label',255);
            $table->string('name',255);
            $table->string('description',255);
            $table->integer('base_type');
            $table->integer('numbering');
            $table->string('prefix',255);
            $table->string('suffix',255);
            $table->integer('zero_padding');
            $table->integer('restriction_bankcash');
            $table->string('softdelete',3);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('entrytypes');
    }
}
