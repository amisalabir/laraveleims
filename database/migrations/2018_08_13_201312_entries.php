<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('tag_id');
            $table->bigInteger('branch_id');
            $table->bigInteger('entrytype_id');
            $table->bigInteger('number');
            $table->date('date');
            $table->decimal('dr_total',25,2);
            $table->decimal('cr_total',25,2);
            $table->string('narration',500);
            $table->string('softdelete',3);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       // Schema::dropIfExists('entries');
    }
}
