<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Entryitems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entryitems', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('entry_id');
            $table->bigInteger('ledger_id');
            $table->decimal('amount',25,2);
            $table->string('dc',1);
            $table->date('reconciliation_date');
            $table->string('softdelete',3);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('entryitems');
    }
}
