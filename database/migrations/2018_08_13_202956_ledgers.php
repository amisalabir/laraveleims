<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ledgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('group_id');
            $table->string('name',255);
            $table->string('code',255);
            $table->decimal('op_balance',25,2);
            $table->decimal('op_balance_dc',25,2);
            $table->integer('type');
            $table->integer('reconciliation');
            $table->string('note',500);
            $table->string('softdelete',3);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('ledgers');
    }
}
