<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Accountheads extends Model
{
           //fillable fields
    protected $fillable = ['id','name'];
    
    //custom timestamps name
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
