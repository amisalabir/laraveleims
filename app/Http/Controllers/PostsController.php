<?php

namespace App\Http\Controllers;
use App\Post;
use DB;
use App\Accountheads;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Session;

class PostsController extends Controller
{
    public function index(){
        //fetch all posts data
        $posts = Post::orderBy('created','desc')->get();
        
        //pass posts data to view and load list view
        return view('posts.index', ['posts' => $posts]);
    }

    public static function selectView($listData){
        $accountheads='';
        $sql='';
        if($listData=='acchead'){ $sql="select * from accountheads where softdelete='No'"; }
        if($listData=='entrytype'){ $sql="select * from entrytypes where softdelete='No'"; }
        if($listData=='branch'){ $sql="select * from branch where softdelete='No'"; }
        if($listData=='bank'){$sql="select  id,name, accountname from bank WHERE softdelete='No' ORDER by id";}
         $accountheads= DB::select($sql);
        return $accountheads;
    }

    
    public function details($id){
        //fetch post data
        $post = Post::find($id);
        
        //pass posts data to view and load list view
        return view('posts.details', ['post' => $post]);
    }
    
    public function add(){
        //load form view
        return view('posts.add');
    }
       
    public function entry(){
        //load form view
        return view('posts.entry');
    }
    
    public function insert(Request $request){
              
        if($_POST['addTransaction']=='addTransaction'){

             echo "<pre>"; var_dump($_POST);  echo "</pre>";
       
die();
        }

    else{
        //validate post data
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        
        //get post data
        $postData = $request->all();
        
        //insert post data
        Post::create($postData);
        
        //store status message
        Session::flash('success_msg', 'Post added successfully!');

        return redirect()->route('posts.index');
        
         //$postData = $request->all();
        //var_dump($postData);
        }
      
    }
    
    public function edit($id){
        //get post data by id
        $post = Post::find($id);
        
        //load form view
        return view('posts.edit', ['post' => $post]);
    }
    
    public function update($id, Request $request){
        //validate post data
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);
        
        //get post data
        $postData = $request->all();
        
        //update post data
        Post::find($id)->update($postData);
        
        //store status message
        Session::flash('success_msg', 'Post updated successfully!');

        return redirect()->route('posts.index');
    }
    
    public function delete($id){
        //update post data
        Post::find($id)->delete();
        
        //store status message
        Session::flash('success_msg', 'Post deleted successfully!');

        return redirect()->route('posts.index');
    }

    public function getlist($listData){

        if(auth()->guard()->guest()):
           return redirect()->route('home');
        else:    
        //fetch all posts data
        $lists='';
        $sql='';
        if($listData=='LEDGER'){$sql="SELECT * FROM `accountheads` WHERE `softdelete`='No'";}
        if($listData=='BANK'){$sql="select  id,accountname  from bank ORDER BY bankname ASC";}
        if($listData=='PARTY'){$sql="select  id,partyname  from party ORDER BY partyname ASC";}
        if($listData=='24' || $listData=='61'){$sql="select  id,name,mobile  from student ORDER BY name ASC";}
        if($listData=='0'){$sql="select  id,partyname  from party ORDER BY partyname ASC";}
        if($listData=='shipexpense'){$sql="select  id, name  from shipexpenses ";}
        if($listData=='book'){$sql="select  value, name from book ORDER by name ASC";}
        if($listData=='STUDENT' || $listData==58 ){$sql="select  id, name,mobile from student ORDER by id";}
        if($listData=='batchid'){$sql="select  id, year from batch ORDER by id";}
        if($listData=='bank'){$sql="select  id,bankname, accountname from bank WHERE softdelete='No' ORDER by id";}
          
        $lists = DB::select($sql);
        $jsonData=json_encode(array('data'=>$lists)); return $jsonData;

        //return Response::json_encode(array('data'=>$lists));
        //return json($lists);
        //pass posts data to view and load list view
        //return view('getlist');
        //return json_encode(array('result' => true,'data' => $lists),200)->setCallback(Input::get('callback'));    
        //return view('posts.getlist', ['listsData' => $lists]);
        //return var_dump($lists);
        endif;
    }



}
