<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eims extends Model
{
       //fillable fields
    protected $fillable = ['name','softdelete'];
    
    //custom timestamps name
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
}
